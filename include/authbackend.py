#!/usr/bin/env python3
#used for sha-256 hashing
import hashlib
#Used for connecting to MySQL
import mysql.connector
#Used for creating random auth strings
import random;
#used for updating the timestamp on those random auth strings
import time;
#used for parsing mode variables and extracting individual booleans from them
from utilparser import *

"""
AUTHENTICATION SCRIPT. WRITTEN MAY-JULY '17 CANNONCONTRAPTION ET. AL.
"""

#TODO: homestuck references aside, this needs to query the database for the salt.
salt = "MmMmM! SoOoO SaLtY! HoNk"

randgen = random.SystemRandom();

connection = None;
cursor = None;

def startConnection(
):
    global connection;
    global cursor;
    if(
            connection != None and
            cursor != None
    ):
        return 1;
    connection = mysql.connector.connect(
        user="fsuauth",
        database="fsucs_authtables",
        password="dtapass1")
    cursor = connection.cursor();
    if connection == None:
        return 2;
    if cursor == None:
        return 3;
    return 0;

def setPermissions(
        user,
        rstring,
        user_to_mod,
        mode
):
    global cursor;
    global connection;
    if(
            check_auth_string(
                get_uid_from_name(user),
                rstring)
            != "ENOAUTH"
    ):
        usermodquery = "update users set permissions = %s where uname = %s";
        userauthquery = "select permissions from users where uname = %(name)s";
        cursor.execute(
            userauthquery,
            {
                'name' : user
            })
        auth_good = False;
        for (permissions) in cursor:
            if parse_exists(
                    int(permissions[0]),
                    16
            ):
                auth_good = True;
        if(auth_good
        ):
            cursor.execute(
                usermodquery,
                (
                    mode,
                    user_to_mod
                ));
            connection.commit();
            return "MODSET_COMPLETE"
        else:
            return "ENOPERMISSION"
    else:
        return "ENOLOGIN"

"""
checkPassword()

Checks the password for the given user against the password database by hashing

Arguments:
    passstring (string)
        The password as sent from the client
    user (string)
        the username to check against

Returns:
    Fail:
        "ENOAUTH"
    Success:
        A random string which the client can use as an auth id for fifteen
        minutes, or however long the check command makes it last.
"""
def checkPassword(
        passstring,
        usern,
        randid=True
):
    global connection;
    global cursor;
    hashthing = hashlib.sha512();
    password = salt + passstring;
    hashthing.update(password.encode("utf-8"));
    passhash = hashthing.hexdigest();
    dbquery = "select pass,uid from users where uname = %(name)s";
    initresult = startConnection();
    if(
            initresult != 0 and
            initresult != 1
    ):
        return 'ENOCONNECTION';
    cursor.execute(
        dbquery,
        {
            'name' : usern
        });
    for (passwd,uid) in cursor:
        if passwd == passhash:
            if randid: return getAuthString(uid);
            else: return "SUCCESS"
    return "ENOAUTH";

def getAuthString(userid):
    part1 = randgen.random()*300000000;
    part2 = randgen.random()*8123673;
    final = str(
        (
            str(part1).encode(
                "ascii",
                "ignore") +
            str(part2).encode("utf-8")
        ));
    rquery = "insert into randomstring(rstring, uid, lasthit, registered) values(%s, %s, %s, %s)";
    cursor.execute(
        rquery,
        (
            final,
            userid,
            int(time.time()),
            int(time.time())
        ));
    connection.commit();
    return str(final);

"""
check_auth_string()

Checks the auth string for validity

Arguments:
    User's ID

Returns:
    Result of auth check
"""
def checkAuthString(
        uid,
        rstring
):
    deadline = time.time()-3600; #1 hour login. (3600s)
    checkquery = "select rstring,uid,lasthit from randomstring where revoked = false and rstring = %s and uid = %s"
    startConnection()
    cursor.execute(
        checkquery,
        (
            rstring,
            uid
        ));
    results = cursor.fetchall();
    for(
            rstring,
            uid,
            lasthit)
    in results:
        if(int(lasthit) >= int(deadline)):
            updatequery = "update randomstring set lasthit = %(time)s where uid = %(usid)s"
            cursor.execute(
                updatequery,
                {
                    'time':int(time.time()),
                    'usid':uid
                });
            connection.commit();
            return True;
    return False;

"""
get_uid_from_name()

Gets the user's ID from the username.
Just for convienience so we can be lazy and not repeatedly write SQL queries everywhere

Arguments:
    uname - the username to check

Retruns: uid - the UID corresponding to the uname
"""
def get_uid_from_name(uname):
    #We set a query
    query = "select uid from users where uname = %(name)s";
    #verify a database connection exists
    istatus = startConnection();
    #and report when it doesn't
    if(istatus > 1):
        return "ENODBCONNECT";
    #then execute the query, passing the username to the SQL where
    cursor.execute(query, {'name':uname});
    #and then scan the results for our answer
    ruid = None;
    for (uid) in cursor:
        ruid = uid;
        #Will return None if there is no user.
    return ruid;

"""
get_permission()

Returns whether the specified user has permission to complete a task. This could be something
like modifying a blog, adding/removing users, etc. This function will look up and verify
user permissions.

arguments:
    uid - the user id to check
    value - the permissions value to check for

returns: "GRANTED" on success
    ENODBCONNECT -- Couldn't connect to the auth DB.
    DENIED -- that user doesn't have permission to do that.
"""
def get_permission(uid, value, blog=None):
    #Standard stuff by now, start with a query
    query = "select permissions from users where uid = %(uid)s";
    #check the DB connection
    istatus = startConnection();
    #report lack of successful DB connection
    if(istatus > 1):
        return "ENODBCONNECT";
    #Execute our query, passing relevant information (UID in this case)
    cursor.execute(query, {'uid': uid});
    #then create a variable to store our result in
    permval = 0;
    #Put our result in there,
    for (permissions) in cursor:
        permval = permissions;
        #and use a parse_exists function to determine whether the permissions number is a component
        #of the mode number. Return appropriately.
    if(parse_exists(permval, value)):
        return "GRANTED";
    #Now we have to check the blog id. If the permission exists for a specific blog, we grant it.
    if blog == None:
        return "DENIED"
    query = "select specialpermissions from postrights where uid = %(uid)s and blogid = %(blog)s";
    cursor.execute(query, {'uid': uid, 'blog': blog});
    for (specialpermissions) in cursor:
        permval = specialpermissions;
    if(parse_exists(permval, value)):
        return "GRANTED"
    return "DENIED"
    
